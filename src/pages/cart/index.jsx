import React from "react";
import { connect } from "react-redux";
import { Link } from "react-router-dom";
import {
  addQuantity,
  removeItem,
  subtractQuantity,
} from "../../store/actions/cartActions";
import classses from "./styles.module.scss";

export const Cart = ({
  items,
  total,
  removeItem,
  addQuantity,
  subtractQuantity,
}) => {
  // to remove the item completely
  const handleRemove = (id) => {
    removeItem(id);
  };
  // to add the quantity
  const handleAddQuantity = (id) => {
    addQuantity(id);
  };
  // to substruct from the quantity
  const handleSubtractQuantity = (id) => {
    subtractQuantity(id);
  };

  return (
    <div className="container">
      <div className="cart">
        <h5>You have ordered:</h5>
        {items.length ? (
          <>
            <ul className={classses.product_list}>
              {items.map((item) => (
                <li className={classses.product_list_item} key={item.id}>
                  <div className={classses.product_list_item_image}>
                    <img src={item.img} alt={item.img} />
                  </div>
                  <div className={classses.product_list_item_description}>
                    <span
                      className={classses.product_list_item_description_title}
                    >
                      {item.title}
                    </span>
                    <span>{item.desc}</span>
                  </div>
                  <div className={classses.product_list_item_price}>
                    <b>
                      Price:
                      {item.price}$
                    </b>
                  </div>
                  <div className={classses.product_list_item_quantity}>
                    <Link to="/cart">
                      <i
                        className="material-icons"
                        onClick={() => {
                          handleAddQuantity(item.id);
                        }}
                      >
                        arrow_drop_up
                      </i>
                    </Link>
                    <b>
                      Quantity:
                      {item.quantity}
                    </b>
                    <Link to="/cart">
                      <i
                        className="material-icons"
                        onClick={() => {
                          handleSubtractQuantity(item.id);
                        }}
                      >
                        arrow_drop_down
                      </i>
                    </Link>
                  </div>
                  <div className={classses.product_list_item_actions}>
                    <button
                      className="waves-effect waves-light btn pink remove"
                      onClick={() => {
                        handleRemove(item.id);
                      }}
                    >
                      Remove
                    </button>
                  </div>
                </li>
              ))}
            </ul>
            <div className={classses.product_list_total}>
              Total:{" "}
              <b>
                ${total} (with shipping ${total + 10})
              </b>
            </div>
          </>
        ) : (
          <p>Nothing.</p>
        )}
      </div>
    </div>
  );
};

const mapStateToProps = (state) => ({
  items: state.addedItems,
  total: state.total,
});
const mapDispatchToProps = (dispatch) => ({
  removeItem: (id) => {
    dispatch(removeItem(id));
  },
  addQuantity: (id) => {
    dispatch(addQuantity(id));
  },
  subtractQuantity: (id) => {
    dispatch(subtractQuantity(id));
  },
});
export default connect(mapStateToProps, mapDispatchToProps)(Cart);
