import React from "react";
import { connect } from "react-redux";
import { addToCart } from "../../store/actions/cartActions";
import Card from "../../components/card";
import classes from "./styles.module.scss";

export const Home = ({ items, addToCart }) => {
  const handleAddToCart = React.useCallback(
    (product) => () => {
      console.log("add");
      addToCart(product.id);
    },
    [addToCart]
  );

  return (
    <div className="container">
      <h3 className="center">Our items</h3>
      <div className={classes.card_list}>
        {items.map((item) => (
          <Card
            key={item.id}
            product={item}
            onAddToCart={handleAddToCart(item)}
          />
        ))}
      </div>
    </div>
  );
};

const mapStateToProps = (state) => ({
  items: state.items,
});
const mapDispatchToProps = (dispatch) => ({
  addToCart: (id) => {
    dispatch(addToCart(id));
  },
});

export default connect(mapStateToProps, mapDispatchToProps)(Home);
