import React from "react";
import classes from "./styles.module.scss";

function Card({ product, onAddToCart = () => {} }) {
  return (
    <div className={classes.container}>
      <div className="card">
        <div className="card-image">
          <img src={product.img} alt={product.title} />
          <span className="card-title">{product.title}</span>
          <span
            to="/"
            className="btn-floating halfway-fab waves-effect waves-light red"
            onClick={onAddToCart}
          >
            <i className="material-icons">add</i>
          </span>
        </div>

        <div className="card-content">
          <p>{product.desc}</p>
          <p>
            <b>
              Price:
              {product.price}$
            </b>
          </p>
        </div>
      </div>
    </div>
  );
}

export default Card;
